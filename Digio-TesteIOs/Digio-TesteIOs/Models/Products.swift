//
//  Products.swift
//  Digio-TesteIOs
//
//  Created by Phelipe Campos on 18/09/18.
//  Copyright © 2018 Lopes. All rights reserved.
//

import UIKit
import ObjectMapper
class Products: Mappable {
    var name: String?
    var bannerURL: String = ""
    var imageURL: String = ""
    var title: String?
    var products: [Products]?
    var categorie: Categorie?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        name    <- map["name"]
        bannerURL         <- map["bannerURL"]
        imageURL         <- map["imageURL"]
        title         <- map["title"]
        
    }
    
    struct Categorie{
        var name: String?
    }
}
