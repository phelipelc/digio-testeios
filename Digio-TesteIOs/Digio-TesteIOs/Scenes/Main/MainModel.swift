//
//  MainModel.swift
//  Digio-TesteIOs
//
//  Created by Phelipe Campos on 18/09/18.
//  Copyright © 2018 Lopes. All rights reserved.
//

import UIKit
import  Alamofire
import Foundation
import ObjectMapper
class MainModel {
    var products = [[Products]]()
    var items: [Products]?
    func requestApi(callback: @escaping (Result<Any>) -> Void){
   
        Alamofire.request("https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products").responseJSON { response in
            switch response.result {
               
                case .success(_):
                    if let hashTags = (response.value as? [String: AnyObject]){
                        for i in 0...hashTags.count-1 {
                        if let dictionary = (Array(hashTags.values)[i]) as? [AnyObject] {
                            print(dictionary[1])
                            self.items = [Products]()
                            for x in 0...dictionary.count-1{
                                if let usr =  Mapper<Products>().map(JSONObject: dictionary[x]){
                                    usr.categorie?.name = usr.title
                                    self.items?.append(usr)
                                }
                            }
                            self.products.append(self.items!)
                        }else{
                            if let user = Mapper<Products>().map(JSONObject: Array(hashTags.values)[i]) {
                                user.categorie?.name = user.title
                                var item = [Products]()
                                item.append(user)
                                self.products.append(item)
                            }
                        }
                    }
                        callback(Result.success(self.products))
                }
                case .failure(_):
                    callback(Result.failure("Erro ao efetuar a requisição." as! Error))
                
                }
                
            
                                 // response serialization result
        }
        
      
    }
        

}
