//
//  MainPresenter.swift
//  Digio-TesteIOs
//
//  Created by Phelipe Campos on 18/09/18.
//  Copyright © 2018 Lopes. All rights reserved.
//

import UIKit

protocol PresenterDelegate {
    func fetchData(produts: [[Products]])
    func showProgress()
    func hideProgress()
   
}
class MainPresenter {
    var delegate: PresenterDelegate?
    var model = MainModel()
    
    init(delegate: PresenterDelegate) {
        self.delegate = delegate
    }
    
    func getData(){
        model.requestApi { (result) in
            switch result{
            case .success(var produtos ):
                let prod = produtos as? [[Products]]
                self.delegate?.fetchData(produts: prod!)
                self.delegate?.hideProgress()
                break
            
            case .failure(let error): break
            }
        }
        
    }
    
}
