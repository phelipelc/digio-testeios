//
//  MainTableViewController.swift
//  Digio-TesteIOs
//
//  Created by Phelipe Campos on 17/09/18.
//  Copyright © 2018 Lopes. All rights reserved.
//

import UIKit
class MainViewController: UIViewController, PresenterDelegate {
    @IBOutlet weak var tableView: UITableView!
   var productos = [[Products]]()
   var presenter: MainPresenter?
   var storedOffsets = [Int: CGFloat]()
    @IBOutlet weak var loading: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        presenter = MainPresenter(delegate: self)
        showProgress()
        
    }
    
    func fetchData(produts: [[Products]]) {
        productos = produts
        tableView.reloadData()
    }
    func showProgress() {
        presenter?.getData()
        loading.startAnimating()
    }
    func hideProgress() {
        loading.stopAnimating()
    }
}
extension MainViewController: UITableViewDataSource, UITableViewDelegate{
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        var i = indexPath.row
        

        return cell
    }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return productos.count
    }
    
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? TableViewCell else { return }
        
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
     func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? TableViewCell else { return }
        
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
    
}


 
   
extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return productos[collectionView.tag].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
       print(collectionView.tag)
       print(indexPath.item)
        
        var produt = productos[collectionView.tag]

        let url = URL(string: (produt[indexPath.item].bannerURL).isEmpty ? produt[indexPath.item].imageURL : produt[indexPath.item].bannerURL)
        let data = try? Data(contentsOf: url!)
        
        if let imageData = data {
            let image = UIImage(data: imageData)
           cell.ivProduct.image = image
        }
    
        cell.ivProduct.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        cell.ivProduct.layer.borderWidth = 2
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }

}

