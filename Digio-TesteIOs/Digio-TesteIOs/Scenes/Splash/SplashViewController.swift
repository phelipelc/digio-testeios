//
//  SplashViewController.swift
//  Digio-TesteIOs
//
//  Created by Phelipe Campos on 18/09/18.
//  Copyright © 2018 Lopes. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loading.startAnimating()
    }

}
